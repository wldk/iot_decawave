### Decawave tag position reader.

This project contains example code, that might be run to collect data from
**Decawave** tag, which is connected via **usb** (*ttyACM\<x\>*)
and write collected data to DB (influxdb is used in this project, for further usage in grafana)

#### Files
* output_parser.py - **Decawave** serial output parser. Parser is written for csv output format (**lec** command to activate)
* read_position.py - process, that collects the data and writes to db
* utils.py - helping files

### Important
Before any work, please visit product page and see the [documentation](https://www.decawave.com/product/mdek1001-deployment-kit/)

#### Usage
Install python virtual environment and create a virtual environment in source directory (pwd is project root folder).

Activate virtual environment, install poetry via **pip** and install dependencies:
```
pip install poetry && poetry install
```

> After diggin in the source code and observing the raw data with [NRF connect](https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp)
> it seems, that the position can be readed from characteristic "003bbdf2-c634-4b3d-ab56-7ec889b89a37"


### Connecting and reading position via BLE
Python modules that might be used:
* [pygatt](https://github.com/peplin/pygatt) :pensive: - after a few samples it seem that it lacks a handler for the disconnection.
* [bleak](https://github.com/hbldh/bleak) :grimacing: - async and does have a handler for the disconnection

> modules were tested on RPi 3
