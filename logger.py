import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def logger_init(logfile_path=None):
    formatter = logging.Formatter(
        '%(asctime)s %(levelname)s %(threadName)s %(filename)s %(funcName)s: %(message)s'
    )
    if logfile_path is not None:
        for handler in (
                logging.StreamHandler(),
                logging.FileHandler(logfile_path,
                                    'a+',
                                    encoding='utf-8',
                                    delay='true'),
        ):
            handler.setLevel(logging.DEBUG)
            handler.setFormatter(formatter)
            logger.addHandler(handler)
    else:
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        handler.setLevel(logging.DEBUG)
        logger.addHandler(handler)
