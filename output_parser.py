from typing import List


def getPosition(output: bytes) -> dict:
    outputString = output.decode()
    positionSubStringIdx = outputString.find('POS')
    if positionSubStringIdx != -1:
        position = outputString[positionSubStringIdx:].split(',')[1:4]
        position = toFloats(position)
        return dict(zip(['x', 'y', 'z'], position))


def getAncorsInfo(output: bytes) -> List[dict]:
    """
    Returns list of dict with info about ancors
    """
    outputString = output.decode().split(',')
    _ancorsInfo = ['x', 'y', 'z']
    _ancorsData = []
    for idx, i in enumerate(outputString):
        if 'AN' in i:
            """
            Getting ancor number, id and it's position
            """
            position = outputString[idx + 2 : idx + 5]
            position = toFloats(position)
            _ancorsData.append(
                (outputString[idx + 1], dict(zip(_ancorsInfo, position)))
            )
    return _ancorsData


def toFloats(positions) -> List[float]:
    return [float(x) for x in positions]


def parseOutput(output: bytes) -> (dict, List[dict]):
    return getPosition(output), getAncorsInfo(output)
