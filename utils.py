from typing import List
from influxdb import InfluxDBClient


def getInfluxClient(
    host='localhost', port=8086, user=None, password=None, db='DecaWave'
) -> InfluxDBClient:
    return InfluxDBClient(
        host=host, port=port, username=user, password=password, database=db
    )


def convertPositionData(data, tagID) -> dict:
    return {'measurement': tagID, 'fields': data}


def convertAncorsData(data) -> List[dict]:
    return [{'measurement': idx, 'fields': point} for idx, point in data]
