from multiprocessing import Process
from signal import SIGINT, SIGTERM, signal
from time import sleep

from serial import Serial

from output_parser import parseOutput
from utils import convertAncorsData, convertPositionData, getInfluxClient

_serial = Serial('/dev/ttyACM0', 115200, timeout=0)


class DecawaveTagError(Exception):
    """
    Exception raised if the tagID is None.
    """

    def __str__(self):
        return 'tagID cannot be None'


class DecawaveTag(Process):
    """
    All commands are from decawave kit documentation
    https://www.decawave.com/product/mdek1001-deployment-kit/
    """

    def __init__(self, _serial: Serial, tagID=None, tag_timeout=2, retry=5):
        super().__init__()
        if tagID is None:
            raise DecawaveTagError()
        self.tagID = tagID
        self.serial = _serial
        self.format = 'lec\r'.encode()
        self.retryCounter = retry
        self.tagTimeout = tag_timeout - 0.05 if tag_timeout - 0.05 > 0 else tag_timeout
        self.influxClient = getInfluxClient()

        signal(SIGINT, self.stop)
        signal(SIGTERM, self.stop)

    def startSerialCommunication(self):
        self.serial.flushOutput()
        self.serial.flushInput()
        self.serial.write(b'\r\r')
        sleep(0.5)
        self.serial.flushInput()

    def run(self):
        retry = 0
        self.startSerialCommunication()
        self.serial.write(self.format)
        sleep(1)
        while retry < self.retryCounter:
            sleep(self.tagTimeout)
            response = self.serial.readline()
            if response == ''.encode():
                retry += 1
                continue
            if (
                response != 'dwm> '.encode()
                and response.decode().strip() != self.format.decode().strip()
            ):
                try:
                    position, ancors = parseOutput(response)
                    self.writeDataToDB(position, ancors)
                    retry = 0
                except Exception as e:
                    self.serial.flushInput()
                    self.serial.flushOutput()
                    print(e)
        self.stop()

    def stop(self, signum=None, sigframe=None):
        self.serial.write(self.format)
        exit(0)

    def writeDataToDB(self, position, ancors):
        position = convertPositionData(position, self.tagID)
        ancors = convertAncorsData(ancors)
        self.influxClient.write_points([position, *ancors])


if __name__ == '__main__':
    tag = DecawaveTag(_serial, tagID='DW8918')
    tag.start()
