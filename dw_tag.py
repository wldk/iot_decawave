import numpy as np
from dataclasses import dataclass, asdict
from utils import getInfluxClient
from logger import logger
from binascii import hexlify


@dataclass
class Position:
    x: float = 0.0
    y: float = 0.0
    z: float = 0.0


class DWTag:
    def __init__(self, mac_id: str, tag_id: str):
        """
        DecaWave Tag Reader
        Class for reading the characteristics of specified (via mac_id argumnet) tag

        Characteristics can be found in source code of the DecaWave:
            https://www.decawave.com/mdek1001/sourcecode/
        """
        self.device = None
        self.macId = mac_id
        self.tagID = tag_id
        self.characteristics = {"POSITION": "003bbdf2-c634-4b3d-ab56-7ec889b89a37"}
        self.subscriptions = {"POSITION": self.storeData}
        self.dbClient = getInfluxClient()

    def storeData(self, sender, data):
        position = self.decodePositionCharacteristic(data)
        if position is not None:
            dataPoint = [{"measurement": self.tagID, "fields": asdict(position)}]
            self.writeToDB(dataPoint)

    def onDisconnection(self):
        pass

    def writeToDB(self, point):
        try:
            self.dbClient.write_points(point)
            logger.debug("Written")
        except Exception as e:
            logger.debug("%s", e)

    @staticmethod
    def decodePositionCharacteristic(byteArray: bytes):
        """
        Checking if the operation mode sends the position data
        If does - converting it to human readable data in metric system
        """
        coordinates = []
        # print(hexlify(byteArray[15:17]))
        # print(hexlify(byteArray[22:24]))
        # print(hexlify(byteArray[31:33]))
        _str = []
        for axis in range(4):
            _str.append(hexlify(byteArray[2 + 7 * axis : 4 + 7 * axis]))
        print(_str)
        if byteArray[0] == 2:
            for axis in range(3):
                """
                Getting 4 bytes from frame as int32 value
                Example:
                    02-[E3-FC-FF-FF]-[71-00 00-00]-[1C-05-00-00]-57-04
                       1             2             3
                       x-----------| y-----------| z-----------|
                """
                coordinateBytes = byteArray[(axis << 2) + 1 : (axis << 2) + 5]
                coordinate = np.frombuffer(coordinateBytes, dtype=np.int32)[0]
                coordinate /= 1000
                coordinates.append(coordinate)
            _str = []
            for axis in range(4):
                _str.append(hexlify(byteArray[15 + 7 * axis : 17 + 7 * axis]))
            print(_str)
            return Position(*coordinates)
