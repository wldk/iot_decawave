import asyncio
from dw_tag import DWTag
from bleak import BleakClient, BleakScanner
from logger import logger_init, logger


async def subscribeOnNotifications(
    mac_id: str,
    characterisitc: str,
    notification_callback: callable,
):
    disconnected_event = asyncio.Event()

    def onDisconnection(client, data):
        logger.debug("Disconnected callback called!")
        logger.debug("%s %s".format(client, data))
        disconnected_event.set()

    while True:
        try:
            device = await BleakScanner.find_device_by_address(mac_id)
            if device is not None:
                async with BleakClient(
                    device, disconnected_callback=onDisconnection
                ) as client:
                    logger.debug("Subscribing to the characterisitc...")
                    await client.start_notify(characterisitc, notification_callback)
                    logger.debug("Sleeping until device disconnects...")
                    await disconnected_event.wait()
                    disconnected_event.clear()
        except Exception as e:
            logger.error("%s", e)


logger_init()
# tag = DWTag(mac_id="cd:6f:72:7c:bf:31", tag_id="dwTag")
tag = DWTag(mac_id="d6:9a:91:65:4a:b9", tag_id="dwTag")
loop = asyncio.get_event_loop()
loop.run_until_complete(
    subscribeOnNotifications(tag.macId, tag.characteristics["POSITION"], tag.storeData)
)
